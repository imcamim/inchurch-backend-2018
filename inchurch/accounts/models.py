from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager)
from django.db import models



class UserManager(BaseUserManager):
    def create_user(self, email, password=None, is_active=True, is_staff=False, is_admin=False):
        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Users must have a password")
        user_obj = self.model(
            email=self.normalize_email(email)
        )
        user_obj.set_password(password)   # change user password
        user_obj.staff=is_staff
        user_obj.admin=is_admin
        user_obj.active=is_active
        user_obj.save(using=self.db)
        return user_obj

    def create_staffuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
            is_staff=True,
        )
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
            is_staff=True,
            is_admin=True,
        )
        return user


class User(AbstractBaseUser):


    # User DB Table
    profile_img = models.ImageField('Profile Image', upload_to='static', blank=True)
    fullname = models.CharField('Full Name', max_length=50)
    email = models.EmailField('Email', unique=True, max_length=50)
    department = models.CharField('Department', max_length=30)
    active = models.BooleanField('Is Active', default=True)
    staff = models.BooleanField('Is Staff', default=False)
    admin = models.BooleanField('Is Admin', default=False)
    created_at = models.DateTimeField('Created at', auto_now_add=True)
    updated_at = models.DateTimeField('Updated at', auto_now=True)

    # email is a username field required by default
    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        if self.fullname:
            return self.fullname
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_active(self):
        return self.active

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'user'
        verbose_name_plural = 'users'


class NewDepartment(models.Model):
    # Department DB Table

    department = models.CharField('Department', max_length=30)
    created_at = models.DateTimeField('Created at', auto_now_add=True)
    updated_at = models.DateTimeField('Updated at', auto_now=True)


    def __str__(self):
        return self.department


    class Meta:
        ordering = ['-created_at']
        verbose_name = 'department'
        verbose_name_plural = 'departments'


from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from inchurch.accounts.models import NewDepartment

User = get_user_model()

class UserRegisterForm(forms.ModelForm):
    # Create a new user with all required fields plus password

    pass1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    pass2 = forms.CharField(label='Repeat Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('profile_img', 'fullname', 'email',  'department', )


    def clean_pass2(self):
        # check if the both pass match

        pass1 = self.cleaned_data.get('pass1')
        pass2 = self.cleaned_data.get('pass2')

        if pass1 and pass2 and pass1 != pass2:
            raise forms.ValidationError("Passwords don't match")
        return pass2

    def save(self, commit=True):
        # Save pass in hashed format

        user = super(UserRegisterForm, self).save(commit=True)
        user.set_password(self.cleaned_data["pass1"])
        user.save()
        return user


class UserChangeForm(forms.ModelForm):
    # Update all fields user. But replace the pass field to the admin's pass hash display field


    class Meta:
        model = User
        fields = ('profile_img', 'fullname', 'email',  'department')




class DepartmentForm(forms.ModelForm):
    # New Department Form

    class Meta:
        model = NewDepartment
        fields = '__all__'

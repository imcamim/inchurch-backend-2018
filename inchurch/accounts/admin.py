from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .forms import UserRegisterForm, UserChangeForm



User = get_user_model()

class UserAdmin(BaseUserAdmin):
    # Add and change user instances

    form = UserChangeForm
    add_form = UserRegisterForm

    list_display = ('email', 'admin')
    list_filter = ('admin', 'staff', 'active')
    fieldsets = (
        (None, {'fields': ('fullname', 'email', 'password')}),
        ('Permissions', {'fields': ('admin', 'staff', 'active')})
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'pass1', 'pass2')}
         ),
    )

    search_fields = ('email', 'fullname')
    ordering = ['email']
    filter_horizontal = ()

admin.site.register(User, UserAdmin)


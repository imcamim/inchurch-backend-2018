from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.urls import reverse
from django.shortcuts import render, redirect

from inchurch.accounts.models import User
from inchurch.accounts.forms import DepartmentForm
from .forms import UserRegisterForm, UserChangeForm


def register(request):
    # Register new user

    if request.method == 'POST':

        form = UserRegisterForm(request.POST)

        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            return redirect('home.html')
        else:
            raise ValueError("An error occurred, please try again.")
    else:
        form = UserRegisterForm()
        args = {'form': form}
        print("Form saved sucefully.")
    return render(request, 'register.html', {'form': form}, args)



@login_required
def full_profile(request, pk=None):
    # Display all user info

    if pk:
        user = User.objects.get(pk=pk)
    else:
        user = request.user
    args = {'user': user}
    return render(request, 'full_profile.html', args)




@login_required
def update_profile(request):
    # Update user profile

    if request.method == 'POST':
        form = UserChangeForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect(reverse('accounts:full_profile'))
    else:
        form = UserChangeForm(instance=request.user)
        args = {'form': form}
        return render(request, 'update_profile.html', args)




@login_required
def delete_profile(request, pk):
    # Delete user profile logged-in

    try:
        user = User.objects.get(pk=pk)
        user.delete()
        messages.success(request, "User deleted successfully!")

    except User.DoesNotExist:
        messages.error(request, "User doesn't exist!")
        return render(request, 'deleted_successfully.html')

    except Exception as e:
        return render(request, 'login.html', {'err': e.message})
    return render(request, 'login.html')



@login_required
def change_pass(request):
    # Update user pass

    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect(reverse('accounts:change_confirmed'))
        else:
            return redirect(reverse('accounts:change_pass'))
    else:
        form = PasswordChangeForm(request.user)

        args = {'form': form}
        return render(request, 'change_pass.html', args)



def create_department(request):
    # create new department

    if request.method == 'POST':
        form = DepartmentForm(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, "New Department saved successfully!")
            return redirect(reverse("accounts:change_confirmed"))
        else:
            messages.error(request, "Please correct the error bellow!")
    else:
        form = DepartmentForm()

        args = {'form': form}
        return render(request, 'create_department.html', args)




def home(request):
    return render(request, 'home.html')

def deleted_successfully(request):
    return render(request, 'deleted_successfully.html')

def change_confirmed(request):
    return render(request, 'change_confirmed.html')

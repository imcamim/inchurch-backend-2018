from django.urls import path
from inchurch.accounts import views
from django.contrib.auth import views as auth_views


app_name = 'accounts'


urlpatterns = [

    path('change_pass/', views.change_pass, name='change_pass'),
    path('create_department/', views.create_department, name='create_department'),
    # path('delete_department/', views.delete_department, name='delete_department'),
    path('delete_profile/<int:pk>/', views.delete_profile, name='delete_profile'),
    path('full_profile/', views.full_profile, name='full_profile'),
    path('', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'),  name='logout'),
    path('register/', views.register, name='register'),
    path('home/', views.home, name='home'),
    path('update_profile/', views.update_profile, name='update_profile'),
    path('deleted_successfully/', views.deleted_successfully, name='deleted_successfully'),
    path('change_confirmed/', views.change_confirmed, name='change_confirmed'),
    ]

# To Run Me


* First clone the repository from your local machine.

#### git clone remote "url adress"

* Create a virtualenv and activate it.
 
 #### python3.7 -m venv myvenv
 #### source myvenv/bin/activate (on ubuntu or Mac command line)
 
* Install requeriments.txt throught the file on this folder

* Set the db throught the comands bellow:

#### python manage.py migrate
#### python manage.py makemigrations

* To run on your browser type:

#### python manage.py runserver

###### On your terminal will open a url open on your browser and run, test register view, login, and anything else on this application


#### Version is: Python3.7 and django 2.1.3
